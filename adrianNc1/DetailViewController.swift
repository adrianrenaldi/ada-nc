//
//  DetailViewController.swift
//  adrianNc1
//
//  Created by Adrian Renaldi on 05/03/20.
//  Copyright © 2020 Adrian Renaldi. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    
    var model = FAQModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewConfig()
        stylingLabel()
    }
    
    func stylingLabel() {
        let maximumLabelSize: CGSize = CGSize(width: 375, height: 9999)
        let expectedLabelSize: CGSize = answerLabel.sizeThatFits(maximumLabelSize)
        var newFrame: CGRect = answerLabel.frame
        newFrame.size.height = expectedLabelSize.height
        answerLabel.frame = newFrame
    }
    
    func viewConfig() {
        questionLabel.text = model.question
        answerLabel.text = model.answer
    }
    
    @IBAction func backToList(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
