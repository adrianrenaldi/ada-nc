//
//  ViewController.swift
//  adrianNc1
//
//  Created by Adrian Renaldi on 05/03/20.
//  Copyright © 2020 Adrian Renaldi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var faqTableView: UITableView!
    @IBOutlet weak var knowMeMoreView: UIView!
    @IBOutlet weak var segmentButton: UISegmentedControl!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var questionTextField: UITextField!
    
    var arrQA = [FAQModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialLoad()
        setupTableViewCell()
        setupData()
    }
    
    func initialLoad() {
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = profileImage.bounds.width / 2
        
        knowMeMoreView.isHidden = true
        knowMeMoreView.layer.cornerRadius = 10
        
        faqTableView.layer.cornerRadius = 10
        
        segmentButton.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
        segmentButton.setTitleTextAttributes([.foregroundColor: UIColor.init(red: 52/255, green: 128/255, blue: 89/255, alpha: 1)], for: .normal)
    }
    
    func setupTableViewCell() {
        faqTableView.register(UINib(nibName: "FAQCell", bundle: nil), forCellReuseIdentifier: "FAQCell")
    }
    
    func setupData() {
        arrQA.append(FAQModel(question: "Where do I live?", answer: "I live in Jakarta. I was born there and grew up in Jakarta."))
        arrQA.append(FAQModel(question: "What is my hobby?", answer: "I do like sports a lot, especially basketball. I started to like basketball since I was in six grade. I watched my senior play basketball in school, and since then I fell in love with it. In junior high, I took basketball extracurricular with my friends. And guess what, I made a team that represent my school. My love with basketball grows continuously until now. Although I never play basketball again recently, I do like to watch NBA. I follow the news, the trade, any changes in teams, and everything about NBA or basketball."))
        arrQA.append(FAQModel(question: "What is your favorite food?", answer: "I like a lot kind of food. But one thing for sure, I really like fried rice."))
        arrQA.append(FAQModel(question: "How do you know programming?", answer: "I like everything about technology. I am amazed about how technology bring us to the point where we live right now. The first thing comes to my mind when we talk about technology is programming. It's pretty narrow-minded, but it is what it is. In the high school, I was introduced by my teacher about programming. The first language that I use in school is Pascal. It taught me how the computational system works. I took Information Technology major in university and I still love programming until now."))
    }
    
    
    @IBAction func changeSegment(_ sender: UISegmentedControl) {
        switch segmentButton.selectedSegmentIndex {
        case 0:
            knowMeMoreView.isHidden = true
            faqTableView.isHidden = false
        case 1:
            knowMeMoreView.isHidden = false
            faqTableView.isHidden = true
        default:
            knowMeMoreView.isHidden = true
            faqTableView.isHidden = false
        }
    }
    
    @IBAction func showAlert(_ sender: UIButton) {
        let name = nameTextField.text!
        let question = questionTextField.text!
        
        let title = "Hi, \(name)"
        let message = "You ask: \(question)\nYour question will be answered shortly"
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQCell", for: indexPath) as! FAQCell
        let backgroundView = UIView()
        backgroundView.backgroundColor = .lightGray
        cell.selectedBackgroundView = backgroundView
        cell.layer.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        cell.faqModel = arrQA[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQA.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailViewController = storyboard?.instantiateViewController(identifier: "detailViewController") as! DetailViewController
        detailViewController.model = arrQA[indexPath.row]
        present(detailViewController, animated: true, completion: nil)
    }
}
