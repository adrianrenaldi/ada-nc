//
//  FAQCell.swift
//  adrianNc1
//
//  Created by Adrian Renaldi on 06/03/20.
//  Copyright © 2020 Adrian Renaldi. All rights reserved.
//

import UIKit

class FAQCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    
    var faqModel: FAQModel? {
        didSet {
            cellConfig()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func cellConfig() {
        guard let obj = faqModel else { return }
        questionLabel.textColor = #colorLiteral(red: 0.2039215686, green: 0.5, blue: 0.3490196078, alpha: 1)
        questionLabel.text = obj.question
    }
}
